﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            float number = 0;
            Console.WriteLine("Please enter a number");
            number = float.Parse(Console.ReadLine());

            for (int i = 0; i < 12; i++)
            {
                Console.WriteLine($"{number} multiplied by {(i + 1)} = {number * (i + 1)}");
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
